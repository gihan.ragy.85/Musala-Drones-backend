package com.musula.backend.drones.service;

public interface AuditLogService {
    void logDroneBatteryLow(Integer droneId);
}
