package com.musula.backend.drones.service;

import com.musula.backend.drones.dto.DroneBatchDTO;
import com.musula.backend.drones.dto.DroneDTO;
import com.musula.backend.drones.model.Drone;

public interface DroneService {
    void register(DroneDTO droneDTO);


    Iterable<Drone> getAllAvailable();

    void updateDrone(Integer droneId, DroneBatchDTO droneDTO);

}
