package com.musula.backend.drones.service;


import com.musula.backend.drones.constant.AppConstants;
import com.musula.backend.drones.dto.DroneBatchDTO;
import com.musula.backend.drones.dto.DroneDTO;
import com.musula.backend.drones.enums.DroneModel;
import com.musula.backend.drones.enums.DroneState;
import com.musula.backend.drones.exception.DroneBadRequestException;
import com.musula.backend.drones.exception.DroneNotFoundException;
import com.musula.backend.drones.model.Drone;
import com.musula.backend.drones.model.Medication;
import com.musula.backend.drones.model.Trip;
import com.musula.backend.drones.repository.DroneRepository;
import com.musula.backend.drones.repository.TripRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Slf4j
@Service
@AllArgsConstructor
public class DroneServiceImpl implements DroneService {

    @Autowired
    private final DroneRepository droneRepository;
    @Autowired
    private final TripRepository tripRepository;


    /**
     * this method registers a drone it checks if the drone limit is exceeded
     * and if the serial number is already registered and then saves the drone
     */
    @Transactional
    @Override
    public void register(DroneDTO droneDTO) {
        if (droneRepository.count() >= 10)
            throw new DroneBadRequestException(AppConstants.DRONE_LIMIT_EXCEEDED);
        if (droneRepository.findBySerialNumber(droneDTO.getSerialNumber()).isPresent())
            throw new DroneBadRequestException(AppConstants.SERIAL_NUMBER_ALREADY_REGISTERED);
        DroneModel model = DroneModel.fromValue(droneDTO.getModel());

        Drone drone = Drone.builder()
                .model(model.ordinal())
                .serialNumber(droneDTO.getSerialNumber())
                .weight(droneDTO.getWeight())
                .batteryCapacity(droneDTO.getBatteryCapacity())
                .state(DroneState.IDLE.ordinal()).build();

        droneRepository.save(drone);
        log.info("Drone registered successfully");
    }


    /**
     * this method gets all available drones with battery capacity greater than 25 and state idle
     */
    @Transactional
    @Override
    public Iterable<Drone> getAllAvailable() {

        return droneRepository.findByBatteryCapacityGreaterThanEqualAndState(25, DroneState.IDLE.ordinal());
    }

    /**
     * this method updates a drone it checks if the drone exists and then updates the drone
     * it also checks if the battery capacity, weight, model, serial number and state are not null and then updates the drone
     * it also checks if the state transition is valid and then updates the drone
     * update drone from idle to loading is done through it API and not through the patch API
     */
    @Transactional
    @Override
    public void updateDrone(Integer droneId, DroneBatchDTO droneBatchDTO) {

        Optional<Drone> droneOptional = droneRepository.findById(droneId);
        if (droneOptional.isEmpty())
            throw new DroneNotFoundException(AppConstants.DRONE_ID_NOT_FOUND);
        Drone drone = droneOptional.get();

        if (droneBatchDTO.getBatteryCapacity() != null)
            drone.setBatteryCapacity(droneBatchDTO.getBatteryCapacity());
        if (droneBatchDTO.getWeight() != null)
            drone.setWeight(droneBatchDTO.getWeight());
        if (droneBatchDTO.getModel() != null)
            drone.setModel(DroneModel.fromValue(droneBatchDTO.getModel()).ordinal());
        if (droneBatchDTO.getSerialNumber() != null)
            drone.setSerialNumber(droneBatchDTO.getSerialNumber());
        if (droneBatchDTO.getState() != null) {
            DroneState newState = DroneState.fromValue(droneBatchDTO.getState());
            DroneState currentState = DroneState.fromValue(drone.getState());
            validateStateTransition(drone, currentState, newState);
            drone.setState(newState.ordinal());
        }
        droneRepository.save(drone);
        log.info("Drone updated successfully");
    }

    /**
     * this method validates the state transition of the drone
     */
    private void validateStateTransition(Drone drone, DroneState currentState, DroneState newState) {
        if (currentState == DroneState.IDLE && newState == DroneState.LOADING) {
            throw new DroneBadRequestException(AppConstants.CHANGE_STATE_THROUGH_API);
        } else if (currentState == DroneState.LOADING && newState != DroneState.LOADED) {
            // check weight and battery
            tripRepository.findByDroneIdAndDroneState(drone.getId(), DroneState.LOADING.ordinal()).ifPresent(trip -> {
                validateDroneAndMedicationItems(drone, trip);
            });
            throw new DroneBadRequestException(AppConstants.FROM_STATE_TO_STATE, currentState.name(), newState.name());
        } else if (currentState == DroneState.LOADED && !(newState == DroneState.DELIVERING)) {
            throw new DroneBadRequestException(AppConstants.FROM_STATE_TO_STATE, currentState.name(), newState.name());
        } else if (currentState == DroneState.DELIVERING && newState != DroneState.DELIVERED) {
            throw new DroneBadRequestException(AppConstants.FROM_STATE_TO_STATE, currentState.name(), newState.name());
        } else if (currentState == DroneState.DELIVERED && newState != DroneState.RETURNING) {
            throw new DroneBadRequestException(AppConstants.FROM_STATE_TO_STATE, currentState.name(), newState.name());
        } else if (currentState == DroneState.RETURNING && newState != DroneState.IDLE) {
            throw new DroneBadRequestException(AppConstants.FROM_STATE_TO_STATE, currentState.name(), newState.name());
        }

    }

    private void validateDroneAndMedicationItems(Drone drone, Trip trip) {
        float totalWeight = 0;
        for (Medication medication : trip.getMedicationSet()) {
            totalWeight += medication.getWeight();
        }
        if (totalWeight > drone.getWeight())
            throw new DroneBadRequestException(AppConstants.TOTAL_WEIGHT_GREATER_THAN_ERROR);
    }


}
