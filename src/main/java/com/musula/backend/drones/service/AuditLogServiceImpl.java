package com.musula.backend.drones.service;

import com.musula.backend.drones.model.AuditLog;
import com.musula.backend.drones.repository.AuditLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class AuditLogServiceImpl implements AuditLogService {

    @Autowired
    private AuditLogRepository auditLogRepository;

    public void logDroneBatteryLow(Integer droneId) {
        String message = "Drone " + droneId + " battery level reached critical threshold.";
        auditLogRepository.save(AuditLog.builder()
                .droneId(droneId)
                .message(message)
                .timestamp(LocalDateTime.now())
                .build());
    }

}