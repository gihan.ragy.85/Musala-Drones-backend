package com.musula.backend.drones.service;


import com.musula.backend.drones.constant.AppConstants;
import com.musula.backend.drones.dto.MedicationItem;
import com.musula.backend.drones.dto.TripDTO;
import com.musula.backend.drones.enums.DroneState;
import com.musula.backend.drones.exception.DroneBadRequestException;
import com.musula.backend.drones.exception.DroneNotFoundException;
import com.musula.backend.drones.model.Drone;
import com.musula.backend.drones.model.Medication;
import com.musula.backend.drones.model.Trip;
import com.musula.backend.drones.repository.DroneRepository;
import com.musula.backend.drones.repository.TripRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;


@Slf4j
@Service
public class TripServiceImpl implements TripService {

    @Autowired
    DroneRepository droneRepository;

    @Autowired
    TripRepository tripRepository;

    @Autowired
    ModelMapper modelMapper;


    @Override
    public void loadDroneWithItems(TripDTO tripDTO) {
        Optional<Drone> droneOptional = droneRepository.findById(tripDTO.getDroneId());
        if (droneOptional.isEmpty())
            throw new DroneNotFoundException(AppConstants.DRONE_ID_NOT_FOUND);
        Drone drone = droneOptional.get();
        if (drone.getState() != DroneState.IDLE.ordinal())
            throw new DroneBadRequestException(AppConstants.DRONE_CANT_LOADED_IN_THIS_STATE);

        if (drone.getBatteryCapacity() < 25)
            throw new DroneBadRequestException(AppConstants.DRONE_LOW_BATTERY);
        Set<Medication> medicationSet = new HashSet<>();
        float totalWeight = 0;
        Trip trip = new Trip();
        for (MedicationItem medicationItem : tripDTO.getMedicationIDList()) {
            Medication medication = modelMapper.map(medicationItem, Medication.class);
            totalWeight += medication.getWeight();
            medicationSet.add(medication);
            medication.setTrip(trip);
        }
        if (totalWeight > drone.getWeight())
            throw new DroneBadRequestException(AppConstants.TOTAL_WEIGHT_GREATER_THAN_ERROR);
        trip.setDrone(drone);
        trip.setMedicationSet(medicationSet);
        tripRepository.save(trip);
        drone.setState(DroneState.LOADED.ordinal());
        droneRepository.save(drone);
    }


}
