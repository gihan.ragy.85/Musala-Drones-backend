package com.musula.backend.drones.service;

import com.musula.backend.drones.constant.AppConstants;
import com.musula.backend.drones.model.Drone;
import com.musula.backend.drones.repository.DroneRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class DroneBatteryScheduler {

    @Autowired
    private DroneRepository droneRepository;

    @Autowired
    private AuditLogServiceImpl auditLogService; // Assuming an AuditLogService for logging


    @Scheduled(fixedRate = 60000) // Schedule every minute (adjust as needed)
    public void checkDroneBatteryLevels() {
        List<Drone> drones = droneRepository.findAll();
        for (Drone drone : drones) {
            checkDroneBattery(drone);
        }
    }

    private void checkDroneBattery(Drone drone) {
        float currentBatteryLevel = drone.getBatteryCapacity();

        if (currentBatteryLevel < AppConstants.BATTERY_THRESHOLD) {
            auditLogService.logDroneBatteryLow(drone.getId());
        }
        log.info("Drone {} battery level: {}%", drone.getId(), currentBatteryLevel);
    }

}