package com.musula.backend.drones.service;

import com.musula.backend.drones.dto.TripDTO;

public interface TripService {


    void loadDroneWithItems(TripDTO tripDTO);

}
