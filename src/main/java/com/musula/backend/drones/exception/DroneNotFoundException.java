package com.musula.backend.drones.exception;

import org.springframework.http.HttpStatus;

public class DroneNotFoundException extends ServiceException {
    public DroneNotFoundException(String code, String message) {
        super(code, message);
        status = HttpStatus.NOT_FOUND;
    }

    public DroneNotFoundException(String code, String message, Object... args) {
        super(code, message, args);
        status = HttpStatus.NOT_FOUND;
    }

    public DroneNotFoundException(String code, Object... args) {
        super(code, args);
        status = HttpStatus.NOT_FOUND;
    }
}
