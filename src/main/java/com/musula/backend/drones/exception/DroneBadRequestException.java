package com.musula.backend.drones.exception;

import org.springframework.http.HttpStatus;

public class DroneBadRequestException extends ServiceException {


    public DroneBadRequestException(String code, String message) {
        super(code, message);
        status = HttpStatus.BAD_REQUEST;
    }


    public DroneBadRequestException(String code, Object... args) {
        super(code, args);
        status = HttpStatus.BAD_REQUEST;
    }
}
