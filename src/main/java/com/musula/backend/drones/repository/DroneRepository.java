package com.musula.backend.drones.repository;

import com.musula.backend.drones.model.Drone;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface DroneRepository extends JpaRepository<Drone, Integer> {

    Optional<Drone> findBySerialNumber(String serialNumber);

    List<Drone> findByBatteryCapacityGreaterThanEqualAndState(int batteryCapacity, int state);

}
