package com.musula.backend.drones.repository;

import com.musula.backend.drones.model.Trip;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface TripRepository extends JpaRepository<Trip, Integer> {
    Optional<Trip> findByDroneIdAndDroneState(Integer droneId, Integer state);


}
