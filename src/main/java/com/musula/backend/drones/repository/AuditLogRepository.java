package com.musula.backend.drones.repository;

import com.musula.backend.drones.model.AuditLog;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AuditLogRepository extends JpaRepository<AuditLog, Integer> {


}
