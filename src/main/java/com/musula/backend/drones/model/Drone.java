package com.musula.backend.drones.model;

import jakarta.persistence.*;
import lombok.*;


@Entity(name = "Drone")
@Table(name = "Drone")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Drone {
    public float weight;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String serialNumber;
    private int model;
    private int state;
    private float batteryCapacity;

}
