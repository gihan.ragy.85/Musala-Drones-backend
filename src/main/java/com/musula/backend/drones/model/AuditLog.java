package com.musula.backend.drones.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Entity(name = "AuditLog")
@Table(name = "audit_log")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class AuditLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer droneId;
    private String message;
    private LocalDateTime timestamp;

    // Getters and Setters...
}