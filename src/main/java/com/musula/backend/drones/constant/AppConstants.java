package com.musula.backend.drones.constant;


/**
 * The Class AppConstants.
 */
public final class AppConstants {


    public static final String GENERAL_ERROR_CODE = "1000";

    public static final String SERIAL_LENGTH_MIN_MAX = "1006";
    public static final String WEIGHT_RANGE_ERROR_MESSAGE = "1007";
    public static final String DRONE_MODEL_VALUE = "1008";
    public static final String STATE_NOT_VALID = "1009";
    public static final String SERIAL_NUMBER_ALREADY_REGISTERED = "1010";
    public static final String INVALID_MEDICATION_NAME = "1011";
    public static final String NOT_BLANK_MEDICATION_NAME = "1012";
    public static final String INVALID_MEDICATION_CODE = "1013";
    public static final String NOT_BLANCK_MEDICATION_CODE = "1014";
    public static final String DRONE_ID_NOT_FOUND = "1016";
    public static final String DRONE_CANT_LOADED_IN_THIS_STATE = "1018";
    public static final String DRONE_LOW_BATTERY = "1019";
    public static final String TOTAL_WEIGHT_GREATER_THAN_ERROR = "1020";
    public static final String DRONE_LIMIT_EXCEEDED = "1021";
    public static final String CHANGE_STATE_THROUGH_API = "1022";
    public static final String FROM_STATE_TO_STATE = "1023";
    public static final int BATTERY_THRESHOLD = 25; // Threshold for state change


    /**
     * To avoid creating instance of AppConstants class.
     */
    private AppConstants() {
        throw new IllegalStateException("AppConstants class are not meant to be instantiated.");
    }


}