package com.musula.backend.drones.dto;

import com.musula.backend.drones.constant.AppConstants;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Positive;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Builder
public class MedicationItem {
    @Pattern(regexp = "^[a-zA-Z0-9_-]*$", message = AppConstants.INVALID_MEDICATION_NAME)
    @NotBlank(message = AppConstants.NOT_BLANK_MEDICATION_NAME)
    private String name;

    @Pattern(regexp = "^[A-Z0-9_]*$", message = AppConstants.INVALID_MEDICATION_CODE)
    @NotBlank(message = AppConstants.NOT_BLANCK_MEDICATION_CODE)
    private String code;

    @Positive(message = AppConstants.WEIGHT_RANGE_ERROR_MESSAGE)
    @Max(value = 500, message = AppConstants.WEIGHT_RANGE_ERROR_MESSAGE)
    private float weight;

    private String imageURL;

}
