package com.musula.backend.drones.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TripDTO {
    private Integer droneId;
    private List<MedicationItem> medicationIDList;
}
