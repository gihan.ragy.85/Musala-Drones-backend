package com.musula.backend.drones.dto;

import com.musula.backend.drones.constant.AppConstants;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class DroneDTO {
    @Positive(message = AppConstants.WEIGHT_RANGE_ERROR_MESSAGE)
    @Max(value = 500, message = AppConstants.WEIGHT_RANGE_ERROR_MESSAGE)
    public float weight;
    @Size(min = 1, max = 100, message = AppConstants.SERIAL_LENGTH_MIN_MAX)
    private String serialNumber;
    @NotNull(message = AppConstants.DRONE_MODEL_VALUE)
    @NotNull(message = AppConstants.DRONE_MODEL_VALUE)
    @Schema(description = "Model of the drone", allowableValues = {"Lightweight", "Middleweight", "Cruiserweight", "Heavyweight"})
    private String model;
    @Positive(message = AppConstants.WEIGHT_RANGE_ERROR_MESSAGE)
    @Max(value = 100, message = AppConstants.WEIGHT_RANGE_ERROR_MESSAGE)
    private Float batteryCapacity;

}
