package com.musula.backend.drones.enums;

import com.musula.backend.drones.constant.AppConstants;
import com.musula.backend.drones.exception.DroneBadRequestException;

public enum DroneState {

    IDLE,
    LOADING,
    LOADED,
    DELIVERING,
    DELIVERED,
    RETURNING;


    public static DroneState fromValue(String value) {
        for (DroneState state : DroneState.values()) {
            if (state.name().equalsIgnoreCase(value)) {
                return state;
            }
        }
        throw new DroneBadRequestException(AppConstants.STATE_NOT_VALID);
    }

    public static DroneState fromValue(int value) {
        for (DroneState state : DroneState.values()) {
            if (state.ordinal() == value) {
                return state;
            }
        }
        throw new DroneBadRequestException(AppConstants.STATE_NOT_VALID);
    }
}
