package com.musula.backend.drones.enums;

import com.musula.backend.drones.constant.AppConstants;
import com.musula.backend.drones.exception.DroneBadRequestException;

public enum DroneModel {
    Lightweight,
    Middleweight,
    Cruiserweight,
    Heavyweight;


    public static DroneModel fromValue(String value) {
        for (DroneModel model : DroneModel.values()) {
            if (model.name().equalsIgnoreCase(value)) {
                return model;
            }
        }
        throw new DroneBadRequestException(AppConstants.DRONE_MODEL_VALUE);
    }


}