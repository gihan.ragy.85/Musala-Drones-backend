package com.musula.backend.drones.controller;

import com.musula.backend.drones.dto.TripDTO;
import com.musula.backend.drones.service.TripService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@CrossOrigin
@RestController
@RequestMapping("/api/v1/drone")
public class TripController {

    @Autowired
    TripService droneService;

    @Operation(summary = "status = 400, message = Bad Request with error code " +
            "\n", description = "This API create Load Drone with mediation items")
    @PostMapping(value = "/trip")
    public ResponseEntity<?> loadDroneWithItems(@Valid @RequestBody TripDTO tripCreater) {
        droneService.loadDroneWithItems(tripCreater);
        return ResponseEntity.accepted().build();
    }

}
