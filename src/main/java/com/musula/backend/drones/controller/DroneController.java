package com.musula.backend.drones.controller;

import com.musula.backend.drones.dto.DroneBatchDTO;
import com.musula.backend.drones.dto.DroneDTO;
import com.musula.backend.drones.model.Drone;
import com.musula.backend.drones.service.DroneService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@CrossOrigin
@RestController
@RequestMapping("/api/v1/drone")
public class DroneController {

    @Autowired
    DroneService droneService;

    @Operation(summary = "status = 400, message = Bad Request with error code " +
            "\n", description = "This API is used to register new drone")
    @PostMapping("/register")
    public ResponseEntity<?> register(@Valid @RequestBody DroneDTO droneDTO) {
        droneService.register(droneDTO);
        return ResponseEntity.accepted().build();

    }


    @Operation(summary = "status = 400, message = Bad Request with error code " +
            "\n", description = "This API create Load Drone with mediation items")
    @GetMapping(value = "/available")
    public ResponseEntity<?> getAvailableDrones() {
        List<Drone> drone = new ArrayList<>();
        droneService.getAllAvailable().forEach(drone::add);
        return ResponseEntity.ok(drone);
    }

    @Operation(summary = "status = 400, message = Bad Request with error code " +
            "\n", description = "This API is used to update drone")
    @PutMapping(value = "/update/{droneId}")
    public ResponseEntity<?> updateDrone(@PathVariable Integer droneId, @Valid @RequestBody DroneBatchDTO droneBatchDTO) {
        droneService.updateDrone(droneId, droneBatchDTO);
        return ResponseEntity.accepted().build();
    }

}
