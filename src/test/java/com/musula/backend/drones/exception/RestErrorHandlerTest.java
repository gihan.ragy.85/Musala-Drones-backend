package com.musula.backend.drones.exception;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class RestErrorHandlerTest {

    @Mock
    private MessageSource messageSource;

    @InjectMocks
    private RestErrorHandler restErrorHandler;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void handleGeneralException() {
        Exception exception = new Exception("Test exception");
        ResponseEntity<?> responseEntity = restErrorHandler.handleGeneralException(exception);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
        ServiceResponse serviceResponse = (ServiceResponse) responseEntity.getBody();
        assertEquals("Test exception", serviceResponse.getDetails());
    }


    @Test
    void testProcessValidationError() {
        Object target = new Object();
        String objectName = "objectName";
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(target, objectName);
        MethodArgumentNotValidException ex = new MethodArgumentNotValidException(null, bindingResult);
        ResponseEntity<?> responseEntity = restErrorHandler.processValidationError(ex);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        ServiceResponse serviceResponse = (ServiceResponse) responseEntity.getBody();
        assertEquals(0, serviceResponse.getValidationErrorDTO().getFieldErrors().size());
    }

    @Test
    void processServiceException() {
        ServiceException serviceException = new ServiceException("Test exception", "error.code");
        serviceException.setStatus(HttpStatus.BAD_REQUEST);
        when(messageSource.getMessage(anyString(), any(), anyString(), any())).thenReturn("Test error message");

        ResponseEntity<?> responseEntity = restErrorHandler.processServiceException(serviceException);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        ServiceResponse serviceResponse = (ServiceResponse) responseEntity.getBody();
        assertEquals("Test error message", serviceResponse.getDetails());
    }
    @Test
    void resolveLocalizedErrorMessage() {
        when(messageSource.getMessage(anyString(), any(), anyString(), any())).thenReturn("Test error message");

        String errorMessage = restErrorHandler.resolveLocalizedErrorMessage("error.code");

        assertEquals("Test error message", errorMessage);
    }
}