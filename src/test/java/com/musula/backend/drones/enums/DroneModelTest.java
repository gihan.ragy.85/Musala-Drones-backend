package com.musula.backend.drones.enums;

import com.musula.backend.drones.constant.AppConstants;
import com.musula.backend.drones.exception.DroneBadRequestException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DroneModelTest {

    @Test
    void fromValueValid() {
        assertEquals(DroneModel.Lightweight, DroneModel.fromValue("Lightweight"));
        assertEquals(DroneModel.Middleweight, DroneModel.fromValue("Middleweight"));
        assertEquals(DroneModel.Cruiserweight, DroneModel.fromValue("Cruiserweight"));
        assertEquals(DroneModel.Heavyweight, DroneModel.fromValue("Heavyweight"));
    }

    @Test
    void fromValueInvalid() {
        DroneBadRequestException exception = assertThrows(DroneBadRequestException.class, () -> {
            DroneModel.fromValue("Invalid");
        });

        String exceptedCode = AppConstants.DRONE_MODEL_VALUE;
        String actualMessage = exception.getCode();

        //assert code
        assertEquals(exceptedCode, actualMessage);
    }
}