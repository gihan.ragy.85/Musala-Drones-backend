package com.musula.backend.drones.enums;

import com.musula.backend.drones.constant.AppConstants;
import com.musula.backend.drones.exception.DroneBadRequestException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DroneStateTest {

    @Test
    void fromValueString() {
        assertEquals(DroneState.IDLE, DroneState.fromValue("IDLE"));
        assertEquals(DroneState.LOADING, DroneState.fromValue("LOADING"));
        assertEquals(DroneState.LOADED, DroneState.fromValue("LOADED"));
        assertEquals(DroneState.DELIVERING, DroneState.fromValue("DELIVERING"));
        assertEquals(DroneState.DELIVERED, DroneState.fromValue("DELIVERED"));
        assertEquals(DroneState.RETURNING, DroneState.fromValue("RETURNING"));
    }

    @Test
    void fromValueStringInvalid() {
        DroneBadRequestException exception = assertThrows(DroneBadRequestException.class, () -> {
            DroneState.fromValue("Invalid");
        });

        String exceptedCode = AppConstants.STATE_NOT_VALID;
        String actualCode = exception.getCode();

// assert code
        assertEquals(exceptedCode, actualCode);
    }


    @Test
    void fromValueInt() {
        assertEquals(DroneState.IDLE, DroneState.fromValue(0));
        assertEquals(DroneState.LOADING, DroneState.fromValue(1));
        assertEquals(DroneState.LOADED, DroneState.fromValue(2));
        assertEquals(DroneState.DELIVERING, DroneState.fromValue(3));
        assertEquals(DroneState.DELIVERED, DroneState.fromValue(4));
        assertEquals(DroneState.RETURNING, DroneState.fromValue(5));
    }

    @Test
    void fromValueIntInvalid() {
        DroneBadRequestException exception = assertThrows(DroneBadRequestException.class, () -> {
            DroneState.fromValue(6);
        });

        String expectedCode= AppConstants.STATE_NOT_VALID;
        String actualCode = exception.getCode();
        // assert code
        assertEquals(expectedCode, actualCode);

    }
}