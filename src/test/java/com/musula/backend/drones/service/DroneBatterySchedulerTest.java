package com.musula.backend.drones.service;

import com.musula.backend.drones.model.Drone;
import com.musula.backend.drones.repository.DroneRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.mockito.Mockito.*;

class DroneBatterySchedulerTest {

    @Mock
    private DroneRepository droneRepository;

    @Mock
    private AuditLogServiceImpl auditLogService;

    @InjectMocks
    private DroneBatteryScheduler droneBatteryScheduler;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void checkDroneBatteryLevels() {
        Drone drone1 = new Drone();
        drone1.setId(1);
        drone1.setBatteryCapacity(50);

        Drone drone2 = new Drone();
        drone2.setId(2);
        drone2.setBatteryCapacity(20);

        when(droneRepository.findAll()).thenReturn(Arrays.asList(drone1, drone2));

        droneBatteryScheduler.checkDroneBatteryLevels();

        verify(auditLogService, times(1)).logDroneBatteryLow(2);
    }
}