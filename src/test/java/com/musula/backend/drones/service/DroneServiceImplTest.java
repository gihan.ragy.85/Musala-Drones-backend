package com.musula.backend.drones.service;

import com.musula.backend.drones.constant.AppConstants;
import com.musula.backend.drones.dto.DroneBatchDTO;
import com.musula.backend.drones.dto.DroneDTO;
import com.musula.backend.drones.dto.MedicationItem;
import com.musula.backend.drones.dto.TripDTO;
import com.musula.backend.drones.enums.DroneModel;
import com.musula.backend.drones.enums.DroneState;
import com.musula.backend.drones.exception.DroneBadRequestException;
import com.musula.backend.drones.model.Drone;
import com.musula.backend.drones.repository.DroneRepository;
import com.musula.backend.drones.repository.MedicationRepository;
import com.musula.backend.drones.repository.TripRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class DroneServiceImplTest {

    public static final String SERIAL_NUMBER = "123456";
    @Mock
    private DroneRepository droneRepository;




    @InjectMocks
    private DroneServiceImpl droneService;

    @Captor
    private ArgumentCaptor<Drone> droneCaptor;

    @Test
    public void testRegisterDrone() {
        when(droneRepository.count()).thenReturn(5L);
        when(droneRepository.findBySerialNumber(anyString())).thenReturn(Optional.empty());
        when(droneRepository.save(any(Drone.class))).thenAnswer(i -> i.getArguments()[0]);
        DroneDTO droneDTO = createDroneDTO();
        droneService.register(droneDTO);
        verify(droneRepository, times(1)).count();
        verify(droneRepository, times(1)).findBySerialNumber(anyString());
        verify(droneRepository, times(1)).save(any(Drone.class));
    }

    @Test
    public void testRegisterDroneLimitExceeded() {
        when(droneRepository.count()).thenReturn(10L);
        when(droneRepository.count()).thenReturn(10L);
        DroneDTO droneDTO = createDroneDTO();
        try {
            //need to check exception in other way and check if the save method is not called
            droneService.register(droneDTO);
            fail("Expected DroneBadRequestException to be thrown");
        } catch (DroneBadRequestException e) {
            verify(droneRepository, times(1)).count();
            verify(droneRepository, never()).findBySerialNumber(anyString());
            verify(droneRepository, never()).save(any(Drone.class));
            assert e.getCode().equals(AppConstants.DRONE_LIMIT_EXCEEDED);
        }

    }

    //test for serial number already registered
    @Test
    public void testRegisterSerialNumberAlreadyRegistered() {
        when(droneRepository.count()).thenReturn(5L);
        when(droneRepository.findBySerialNumber(anyString())).thenReturn(Optional.of(new Drone()));
        DroneDTO droneDTO = createDroneDTO();
        try {
            //need to check exception in other way and check if the save method is not called
            droneService.register(droneDTO);
            fail("Expected DroneBadRequestException to be thrown");
        } catch (DroneBadRequestException e) {
            verify(droneRepository, times(1)).count();
            verify(droneRepository, times(1)).findBySerialNumber(anyString());
            verify(droneRepository, never()).save(any(Drone.class));
            assert e.getCode().equals(AppConstants.SERIAL_NUMBER_ALREADY_REGISTERED);
        }

    }

    //test for get all available drones
    @Test
    public void testGetAllAvailable() {
        when(droneRepository.findByBatteryCapacityGreaterThanEqualAndState(anyInt(), anyInt())).thenReturn(List.of(createDrone(100, 400.0f, DroneState.IDLE.ordinal())));
        Iterable<Drone> droneIterable = droneService.getAllAvailable();
        verify(droneRepository, times(1)).findByBatteryCapacityGreaterThanEqualAndState(anyInt(), anyInt());
        assertEquals(droneIterable.iterator().next().getBatteryCapacity(), 100.0f);
    }

    //test  update drone
    @Test
    public void testUpdateDrone() {
        when(droneRepository.findById(anyInt())).thenReturn(Optional.of(createDrone(100, 400.0f, DroneState.IDLE.ordinal())));
        // Act UPDATE BATTERY CAPACITY
        DroneBatchDTO droneBatchDTO = DroneBatchDTO.builder().batteryCapacity(80f).build();

        droneService.updateDrone(1, droneBatchDTO);
        verify(droneRepository, times(1)).findById(anyInt());

        verify(droneRepository, times(1)).save(droneCaptor.capture());

        Drone savedDrone = droneCaptor.getValue();
        assertEquals(savedDrone.getBatteryCapacity(), 80f);
        assertEquals(savedDrone.getWeight(), 400.0f);
        assertEquals(savedDrone.getModel(), DroneModel.Heavyweight.ordinal());
        assertEquals(savedDrone.getSerialNumber(), SERIAL_NUMBER);
        assertEquals(savedDrone.getState(), DroneState.IDLE.ordinal());


    }


    private DroneDTO createDroneDTO() {
        return DroneDTO.builder().model(DroneModel.Heavyweight.name()).serialNumber(SERIAL_NUMBER).weight(2.0f).batteryCapacity(23f).build();

    }

    //create tripdto 2 medication items
    private TripDTO createTripDTO() {
        TripDTO tripDTO = new TripDTO();
        tripDTO.setDroneId(1);
        List<MedicationItem> medicationList = List.of(createMedicationItem("med1", "code1", 100), createMedicationItem("med2", "code2", 200));
        tripDTO.setMedicationIDList(medicationList);
        return tripDTO;
    }

    private MedicationItem createMedicationItem(String medicationName, String code, float weight) {
        return MedicationItem.builder().name(medicationName).code(code).weight(weight).build();

    }

    //create drone
    private Drone createDrone(float batteryCapacity, float weight, int state) {
        return Drone.builder().id(1).model(DroneModel.Heavyweight.ordinal()).serialNumber(SERIAL_NUMBER).weight(weight).batteryCapacity(batteryCapacity).state(state).build();
    }

}