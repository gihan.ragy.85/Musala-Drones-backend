package com.musula.backend.drones.service;

import com.musula.backend.drones.constant.AppConstants;
import com.musula.backend.drones.dto.MedicationItem;
import com.musula.backend.drones.dto.TripDTO;
import com.musula.backend.drones.enums.DroneModel;
import com.musula.backend.drones.enums.DroneState;
import com.musula.backend.drones.exception.DroneBadRequestException;
import com.musula.backend.drones.model.Drone;
import com.musula.backend.drones.model.Trip;
import com.musula.backend.drones.repository.DroneRepository;
import com.musula.backend.drones.repository.MedicationRepository;
import com.musula.backend.drones.repository.TripRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TripServiceImplTest {

    @Mock
    private DroneRepository droneRepository;


    @Mock
    private TripRepository tripRepository;

    @InjectMocks
    private TripServiceImpl tripService;

    @Spy
    private ModelMapper modelMapper = new ModelMapper();

    @Test
    public void testLoadDrone() {
        // Arrange

        TripDTO tripDTO = createTripDTO();

        when(droneRepository.findById(anyInt())).thenReturn(Optional.of(createDrone(100, 400.0f, DroneState.IDLE.ordinal())));
        // Act
        tripService.loadDroneWithItems(tripDTO);


        // Assert
        verify(droneRepository, times(1)).findById(anyInt());
        verify(tripRepository, times(1)).save(any(Trip.class));
    }

    @Test
    public void testLoadDroneWeightException() {
        // Arrange

        TripDTO tripDTO = createTripDTO();

        when(droneRepository.findById(anyInt())).thenReturn(Optional.of(createDrone(100, 200.0f, DroneState.IDLE.ordinal())));
        // Act
        // try catch block to catch the exception
        try {
            tripService.loadDroneWithItems(tripDTO);
            fail("Exception not thrown");
        } catch (DroneBadRequestException e) {
            // Assert
            verify(droneRepository, times(1)).findById(anyInt());
            verify(tripRepository, times(0)).save(any(Trip.class));
            verify(droneRepository, times(0)).save(any(Drone.class));
            //assert code
            assertEquals(e.getCode(), AppConstants.TOTAL_WEIGHT_GREATER_THAN_ERROR);

        }
    }

    //test for battery capacity
    @Test
    public void testLoadDroneBatteryException() {
        // Arrange

        TripDTO tripDTO = createTripDTO();

        when(droneRepository.findById(anyInt())).thenReturn(Optional.of(createDrone(20, 400.0f, DroneState.IDLE.ordinal())));
        // Act
        // try catch block to catch the exception
        try {
            tripService.loadDroneWithItems(tripDTO);
            fail("Exception not thrown");
        } catch (DroneBadRequestException e) {
            // Assert
            verify(droneRepository, times(1)).findById(anyInt());
            verify(tripRepository, times(0)).save(any(Trip.class));
            verify(droneRepository, times(0)).save(any(Drone.class));
            //assert code
            assertEquals(e.getCode(), AppConstants.DRONE_LOW_BATTERY);

        }
    }

    //test for drone state
    @Test
    public void testLoadDroneStateException() {
        // Arrange

        TripDTO tripDTO = createTripDTO();

        when(droneRepository.findById(anyInt())).thenReturn(Optional.of(createDrone(100, 400.0f, DroneState.LOADING.ordinal())));

        // Act
        // try catch block to catch the exception
        try {
            tripService.loadDroneWithItems(tripDTO);
            fail("Exception not thrown");
        } catch (DroneBadRequestException e) {
            // Assert
            verify(droneRepository, times(1)).findById(anyInt());
            verify(tripRepository, times(0)).save(any(Trip.class));
            verify(droneRepository, times(0)).save(any(Drone.class));
            //assert code
            assertEquals(e.getCode(), AppConstants.DRONE_CANT_LOADED_IN_THIS_STATE);

        }
    }


    //create tripdto 2 medication items
    private TripDTO createTripDTO() {
        TripDTO tripDTO = new TripDTO();
        tripDTO.setDroneId(1);
        List<MedicationItem> medicationList = List.of(createMedicationItem("med1", "code1", 100), createMedicationItem("med2", "code2", 200));
        tripDTO.setMedicationIDList(medicationList);
        return tripDTO;
    }

    private MedicationItem createMedicationItem(String medicationName, String code, float weight) {
        return MedicationItem.builder().name(medicationName).code(code).weight(weight).build();

    }

    //create drone
    private Drone createDrone(float batteryCapacity, float weight, int state) {
        return Drone.builder().id(1).model(DroneModel.Heavyweight.ordinal()).serialNumber("123456").weight(weight).batteryCapacity(batteryCapacity).state(state).build();
    }
}