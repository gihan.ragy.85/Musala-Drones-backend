package com.musula.backend.drones.service;

import com.musula.backend.drones.model.AuditLog;
import com.musula.backend.drones.repository.AuditLogRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;

class AuditLogServiceImplTest {

    @Mock
    private AuditLogRepository auditLogRepository;

    @InjectMocks
    private AuditLogServiceImpl auditLogService;

    @Captor
    private ArgumentCaptor<AuditLog> auditLogCaptor;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void logDroneBatteryLow() {
        Integer droneId = 1;
        auditLogService.logDroneBatteryLow(droneId);

        verify(auditLogRepository).save(auditLogCaptor.capture());
        AuditLog savedAuditLog = auditLogCaptor.getValue();

        assertEquals(droneId, savedAuditLog.getDroneId());
        assertEquals("Drone " + droneId + " battery level reached critical threshold.", savedAuditLog.getMessage());
    }
}