package com.musula.backend.drones;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = {"classpath:application-dev.properties"})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DroneControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void aListAlertsWithCustomerIDAsAdmin() throws Exception {
        mockMvc.perform(get("/api/v1/drone/available")
                        .contentType("application/json"))
                .andExpect(status().isOk());
    }
}