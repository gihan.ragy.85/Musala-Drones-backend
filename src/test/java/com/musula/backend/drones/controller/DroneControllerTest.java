package com.musula.backend.drones.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.musula.backend.drones.dto.DroneBatchDTO;
import com.musula.backend.drones.dto.DroneDTO;
import com.musula.backend.drones.enums.DroneModel;
import com.musula.backend.drones.enums.DroneState;
import com.musula.backend.drones.model.Drone;
import com.musula.backend.drones.service.DroneService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class DroneControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DroneService droneService;


    @Test
    public void testRegister() throws Exception {
        DroneDTO droneDTO = DroneDTO.builder().serialNumber("TEST").weight(25f).model(DroneModel.Lightweight.name()).batteryCapacity(25f).build();
        // Set properties for droneDTO

        mockMvc.perform(post("/api/v1/drone/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(droneDTO)))
                .andExpect(status().isAccepted());
    }

    @Test
    public void testGetAvailableDrones() throws Exception {
        when(droneService.getAllAvailable()).thenReturn(Arrays.asList(new Drone(), new Drone()));

        mockMvc.perform(get("/api/v1/drone/available")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdateDrone() throws Exception {
        DroneBatchDTO droneBatchDTO = DroneBatchDTO.builder()
                .serialNumber("TEST")
                .model(DroneModel.Lightweight.name())
                .batteryCapacity(25f)
                .weight(25f)
                .state(DroneState.IDLE.name())
                .build();
        // Set properties for droneBatchDTO

        mockMvc.perform(put("/api/v1/drone/update/{droneId}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(droneBatchDTO)))
                .andExpect(status().isAccepted());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}