package com.musula.backend.drones.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.musula.backend.drones.dto.TripDTO;
import com.musula.backend.drones.service.TripService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TripControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TripService tripService;


    @Test
    public void testLoadDroneWithItems() throws Exception {
        TripDTO tripDTO = new TripDTO();
        // Set properties for tripDTO

        mockMvc.perform(post("/api/v1/drone/trip")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(tripDTO)))
                .andExpect(status().isAccepted());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}