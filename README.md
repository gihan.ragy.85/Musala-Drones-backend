# Drones

Drone Project is a Spring Boot Java application, and the build tool is Maven. The Drone Project is a multi-language project, and you can specify languages under the `translation` folder. Flyway is used for data migration, and Swagger is used for API documentation and testing.

The web server port number is 8044; you can change this in `application.properties`. An H2 in-memory database is used for storing objects.

## Project Assumptions

### Drone Models
Drone models are represented as static integer values:
- Lightweight (0)
- Middleweight (1)
- Cruiserweight (2)
- Heavyweight (3)

### Drone Status
Drone statuses are represented as static integer values:
- IDLE (0)
- LOADING (1)
- LOADED (2)
- DELIVERING (3)
- DELIVERED (4)
- RETURNING (5)

## Assumptions for Implementation

1. **Framework and Tools**:
    - The application is built using Spring Boot.
    - Dependency injection is managed by Spring's `@Autowired` annotation.
    - The project uses standard Spring Boot components like `@RestController`, `@Service`, `@Repository`, etc.

2. **Data Transfer Objects (DTOs)**:
    - DTOs (`DroneDTO`, `TripDTO`, `MedicationItem`) are used to transfer data between client and server.

3. **Database and Repositories**:
    - There are corresponding JPA repositories (`DroneRepository`, `TripRepository`, `MedicationRepository`) for data persistence.
    - An in-memory database or a locally running database is used for development and testing.

4. **Validation**:
    - Validation annotations (like `@Valid`) are used to ensure that incoming request data is valid.
    - Custom exceptions like `ResourceNotFoundException` are used for error handling.

5. **JSON Format**:
    - All input and output data are in JSON format.

6. **Security**:
    - Basic security (e.g., endpoint protection) is not covered but can be added as needed.

### Drone Management (DroneController and DroneService)
1. **Drone Registration**:
    - The `register` endpoint accepts a `DroneDTO` object to create a new drone.
    - Drones have unique serial numbers, model types, weight limits, battery capacities, and states.

2. **Available Drones**:
    - The `getAvailableDrones` endpoint retrieves drones that are in the `IDLE` state.

3. **Battery Levels**:
    - The `getDroneBatteryLevel` endpoint retrieves the current battery level of a specific drone.

### Trip Management (TripController and TripService)
1. **Loading Drones with Medications**:
    - The `loadDroneWithItems` endpoint accepts a `TripDTO` object which includes drone ID and a list of `MedicationItem` objects.
    - A drone must have a battery level of at least 25% to be loaded with medications.
    - The total weight of the medications must not exceed the drone's weight limit.
    - Upon successful loading, the drone's state changes to `LOADED`.

2. **Retrieving Loaded Medications**:
    - The `getLoadedMedications` endpoint retrieves a list of medications loaded onto a specific drone.

### Specific Assumptions for Implementation
1. **Drone State Management**:
    - The drone has various states: `IDLE`, `LOADING`, `LOADED`, `DELIVERING`, `DELIVERED`, and `RETURNING`.
    - Only drones in the `IDLE` state are available for loading.
    - After loading medications, the drone state changes to `LOADED`.

2. **Medication Handling**:
    - Medications are associated with trips, meaning they are loaded as part of trip creation.
    - Each medication item has a name, weight, code, and image.

3. **Periodic Task for Battery Levels**:
    - A periodic task (not shown in the controllers) is assumed to check drone battery levels and create a history/audit event log for this. This task is implemented separately, likely in a scheduled service.

4. **Error Handling**:
    - Custom exceptions like `ResourceNotFoundException` and standard exceptions like `IllegalStateException` are used for error scenarios.
    - Exception handling is assumed to be configured globally (e.g., using `@ControllerAdvice`).

5. **Trip and Medication Persistence**:
    - Trips and their associated medications are persisted in the database.
    - Relationships between drones, trips, and medications are properly mapped in the entities (not shown but assumed to be handled).

6. **Battery Level and Weight Validations**:
    - Battery level check (`< 25%`) and weight limit validations are performed before loading a drone with medications.

## Build

To build the project, run:
```
mvn clean install
```

## Run

To run the project, execute:
```
java -jar target/Musala-Drones-backend.jar
```

## Test API using Swagger

Access the Swagger UI for API documentation and testing at:
```
http://localhost:8044/swagger-ui/index.html
```

## Check the data in H2 DB

Access the H2 database console at:
http://localhost:8044/h2
```
Use the following settings:
- Driver Class: `org.h2.Driver`
- JDBC URL: `jdbc:h2:mem:testdb`
- User Name: `sa`
- Password: `sa`